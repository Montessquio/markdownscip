import * as wasm from "markdown-to-wikidot";

document.getElementById("convert").addEventListener("click", event => {
    document.getElementById("output").innerHTML = wasm.convert(document.getElementById("input").value);
});