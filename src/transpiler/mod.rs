//! Markdown to Wikidot conversions.

use pulldown_cmark::{Event, Options, Parser, Tag};

pub fn convert(md: &str) -> String {
    ::log("Running Convert...");

    // Stores results as Wikidot-Formatted UTF-8.
    let mut buf: Vec<u8> = Vec::new();

    let mut opts = Options::empty();
    opts.insert(Options::ENABLE_FOOTNOTES);
    opts.insert(Options::ENABLE_TABLES);
    let parser = Parser::new_ext(md, opts);

    let mut state = State { blockquote: false };
    for event in parser {
        buf.append(&mut process_event(event, &mut state));
    }

    ::log("Convert Done");
    return String::from_utf8(buf).unwrap_or("[ERROR C0]".to_owned());
}

struct State {
    pub blockquote: bool,
}

fn process_event(e: Event, s: &mut State) -> Vec<u8> {
    match e {
        Event::Start(tag) => start_tag(tag, s),
        Event::End(tag) => end_tag(tag, s),
        Event::Text(tx) => {
            if s.blockquote {
                let mut to = "> ".as_bytes().to_vec();
                to.append(&mut tx.as_bytes().to_vec());
                return to;
            }
            tx.as_bytes().to_vec()
        }
        Event::Html(s) => format!("[[html]]{}[[/html]]", s).as_bytes().to_vec(),
        Event::InlineHtml(s) => format!("[[html]]{}[[/html]]", s).as_bytes().to_vec(),
        Event::FootnoteReference(s) => s.as_bytes().to_vec(),
        Event::SoftBreak => "\n".as_bytes().to_vec(),
        Event::HardBreak => "\n\n".as_bytes().to_vec(),
    }
}

fn start_tag(t: Tag, s: &mut State) -> Vec<u8> {
    match t {
        Tag::Paragraph => Vec::new(),
        Tag::Rule => "----\n".as_bytes().to_vec(),
        Tag::Header(level) => format!("{} ", "+".repeat(level as usize))
            .as_bytes()
            .to_vec(),
        Tag::BlockQuote => {
            s.blockquote = true;
            Vec::new()
        }
        Tag::CodeBlock(s) => format!("[[code]]\n{}", s).as_bytes().to_vec(),
        Tag::List(n) => {
            if n.is_some() {
                // It's ordered
                return "[[ol]]\n".as_bytes().to_vec();
            } else {
                // Unordered
                return "[[ul]]\n".as_bytes().to_vec();
            }
        }
        Tag::Item => "[[li]]\n".as_bytes().to_vec(),
        Tag::FootnoteDefinition(s) => "[[footnote]]".as_bytes().to_vec(),
        Tag::Table(_alignment) => "[[table]]\n".as_bytes().to_vec(),
        Tag::TableHead => "[[hcell]]\n".as_bytes().to_vec(),
        Tag::TableRow => "[[row]]\n".as_bytes().to_vec(),
        Tag::TableCell => "[[cell]]\n".as_bytes().to_vec(),
        Tag::Emphasis => "//".as_bytes().to_vec(),
        Tag::Strong => "**".as_bytes().to_vec(),
        Tag::Code => "{{".as_bytes().to_vec(),
        Tag::Link(dst, title) => format!("[{} ", dst).as_bytes().to_vec(),
        Tag::Image(dst, title) => format!(
            "|||| [[{} width=\"300px\"]] ||\n||||~ ^^{}^^ ||",
            dst, title
        )
        .as_bytes()
        .to_vec(),
    }
}

fn end_tag(t: Tag, s: &mut State) -> Vec<u8> {
    match t {
        Tag::Paragraph => {
            if s.blockquote {
                return "\n>\n".as_bytes().to_vec();
            }
            "\n\n".as_bytes().to_vec()
        }
        Tag::Rule => Vec::new(),
        Tag::Header(level) => "\n".repeat(level as usize).as_bytes().to_vec(),
        Tag::BlockQuote => {
            s.blockquote = false;
            Vec::new()
        }
        Tag::CodeBlock(s) => "\n[[/code]]\n".as_bytes().to_vec(),
        Tag::List(n) => {
            if n.is_some() {
                // It's ordered
                return "\n[[/ol]]".as_bytes().to_vec();
            } else {
                // Unordered
                return "\n[[/ul]]".as_bytes().to_vec();
            }
        }
        Tag::Item => "\n[[/li]]".as_bytes().to_vec(),
        Tag::FootnoteDefinition(s) => "[[/footnote]]".as_bytes().to_vec(),
        Tag::Table(alignment) => "\n[[/table]]".as_bytes().to_vec(),
        Tag::TableHead => "\n[[/hcell]]".as_bytes().to_vec(),
        Tag::TableRow => "\n[[/row]]".as_bytes().to_vec(),
        Tag::TableCell => "\n[[/cell]]".as_bytes().to_vec(),
        Tag::Emphasis => "//".as_bytes().to_vec(),
        Tag::Strong => "**".as_bytes().to_vec(),
        Tag::Code => "}}".as_bytes().to_vec(),
        Tag::Link(dst, title) => format!(" ]{}", title).as_bytes().to_vec(),
        Tag::Image(dst, title) => Vec::new(),
    }
}
