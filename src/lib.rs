extern crate cfg_if;
extern crate wasm_bindgen;

extern crate pulldown_cmark;

mod utils;

mod transpiler;

use cfg_if::cfg_if;
use wasm_bindgen::prelude::*;

cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

/// Convert a markdown string to a wikidot-compatible string.
#[wasm_bindgen]
pub fn convert(md: &str) -> String {
    transpiler::convert(md)
}