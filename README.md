# Markdown-to-Wikidot

This program compiles markdown to wikidot as a WebAssembly app.

## TODO:

[ ] Blockquotes run over one extra line. This doesn't affect the final
render, but it's annoying.

```
> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
>
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.
>
Normal Text
```
